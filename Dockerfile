FROM alpine:3.14

RUN addgroup -g 1000 -S alpine \
    && adduser -u 1000 -G alpine -h /home/alpine -D alpine

RUN set -xe \
    && apk add -Uu --purge --no-cache curl git openssl nodejs npm \
    && npm install --global yarn \
    && npm install -g @vue/cli @vue/cli-service-global \
    && npm install -g @quasar/cli

USER alpine
WORKDIR /home/alpine/project/
ENTRYPOINT ["quasar"]